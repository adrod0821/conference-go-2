from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}  # empty dict of encoders

    def default(self, o):
        if isinstance(o, self.model):  # if o is an instance of the model
            d = {}  # empty dict
            if hasattr(
                o, "get_api_url"
            ):  # if it has the attribute get api url
                d["href"] = o.get_api_url()  # set href to get api url in dict
            for property in self.properties:  # iterate through properties
                value = getattr(
                    o, property
                )  # get attribute of property which is the key and
                # set its value to equal value
                if (
                    property in self.encoders
                ):  # if the property is in the encoders
                    encoder = self.encoders[
                        property
                    ]  # set encoder equal to the property for the key
                    value = encoder.default(value)
                    # get the encoder dict to be the value if there is no value
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        #   if the object to decode is the same class as what's in the
        #   model property, then
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
        #         * get the value of that property from the model instance
        #           given just the property name
        #         * put it into the dictionary with that property name as
        #           the key
        #     * return the dictionary
        else:
            return super().default(
                o
            )  # return super().default(o)  # From the documentation

    def get_extra_data(
        self, o
    ):  # create template to use in encoder and returns in dict
        return {}
