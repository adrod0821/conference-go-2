from django.http import JsonResponse
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    LocationDetailEncoder,
    LocationListEncoder,
    ConferenceDetailEncoder,
    ConferenceListEncoder,
)

from .acl import get_image, get_weather_data


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            conferences, encoder=ConferenceListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
            conference.location.city, conference.location.state.abbreviation
        )
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Not a valid location"}, status=400
            )
        Conference.objects.filter(id=id).update(**content)

        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()  # get all locations
        return JsonResponse(
            locations, encoder=LocationListEncoder, safe=False
        )  # locations is a list of dictionaries
    else:
        content = json.loads(
            request.body
        )  # convert json from the user to python
        try:
            # get State object and put it in content dict
            state = State.objects.get(
                abbreviation=content["state"]
            )  # getting State object
            content["state"] = state  # putting it in content
        except State.DoesNotExist:  # if it doesn't exist
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )  # send this error

        picture = get_image(content["city"], content["state"])
        print(picture)
        content.update(picture)

        location = Location.objects.create(
            **content
        )  # create the content with ** being multple arguments
        # use detail encoder to make the new location
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":  # if delete method
        # if it deletes something it will add a 1 to the count
        count, _ = Location.objects.filter(
            id=id
        ).delete()  # delete this specific object and count it

        # if count > 0 itll show true and
        # if its true that means it deleted something
        return JsonResponse({"deleted": count > 0})
    else:  # puts the only one left so else
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # update it with the new content
        Location.objects.filter(id=id).update(**content)

        # fetch and show updated content
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
