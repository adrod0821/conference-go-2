from .keys import PEXEL_KEY, OPEN_WEATHER_API_KEY
import requests

import json


def get_image(city, state):
    # header for the needed authorization / api key
    headers = {"Authorization": PEXEL_KEY}
    # url with given city and state as search filters/queries
    url = f"https://api.pexels.com/v1/search/?query={city}+{state}"
    # getting the response by requesting using url and api_key
    resp = requests.get(url, headers=headers)
    # parse json / make it a python dict
    content = json.loads(resp.content)
    # return a specific photo from the list in the url field
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    # url to get lat and lon using city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=2&appid={OPEN_WEATHER_API_KEY}"
    # get resp using url
    resp = requests.get(url)
    # parse geocode api and get lat and lon
    lat = resp.json()[0]["lat"]
    lon = resp.json()[0]["lon"]

    # get resp using url and coords
    resp = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    )
    # parse weather api
    content = json.loads(resp.content)
    # return dict by looking at the documentation
    return {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"],
    }
